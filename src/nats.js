const stan = require("node-nats-streaming");
const { usersCollection } = require("./controllers");
const { createDatabase } = require("./middlewares/database");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */

    const db = createDatabase(mongoClient);

    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const usersSubscription = conn.subscribe("users", opts);

    usersSubscription.on("message", async (msg) => {
      const { eventType, entityId: _id, entityAggregate } = JSON.parse(
        msg.getData()
      );

      if (eventType === "UserCreated") {
        const newUser = { _id, ...entityAggregate };
        await db.collection(usersCollection).insertOne(newUser);
      } else if (eventType === "UserDeleted") {
        await db.collection(usersCollection).deleteOne({ _id });
      }
    });

    /* ******************* */
  });

  return conn;
};
