const { v4: uuid } = require("uuid");
const { validateFields } = require("./validators");

const usersCollection = "users";

async function getAllUsers(req, res) {
  const response = await req.db.collection(usersCollection).find({}).toArray();
  res.status(200).json({ users: response });
}

async function createUser(req, res) {
  const { statusCode, errorMessage } = validateFields(req.body);

  if (errorMessage) {
    res.status(statusCode).json({ error: errorMessage });
    return;
  }

  const id = await generateNewId(req.db);
  const { name, email, password } = req.body;

  const message = JSON.stringify({
    eventType: "UserCreated",
    entityId: id,
    entityAggregate: {
      name,
      email,
      password,
    },
  });
  req.sc.publish("users", message);

  res.status(201).json({ user: { id, name, email } });
}

async function deleteUser(req, res) {
  const id = req.params.uuid;
  await req.db.collection(usersCollection).findOne({ _id: id });

  const message = JSON.stringify({
    eventType: "UserDeleted",
    entityId: id,
    entityAggregate: {},
  });
  req.sc.publish("users", message);

  res.status(200).json({ id });
}

async function generateNewId(db) {
  let id = uuid();
  let user = await db.collection(usersCollection).findOne({ _id: id });

  while (user) {
    id = uuid();
    user = await db.collection(usersCollection).findOne({ _id: id });
  }

  return id;
}

module.exports = { getAllUsers, createUser, deleteUser, usersCollection };
