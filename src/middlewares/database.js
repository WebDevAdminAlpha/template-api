function createMongodbMiddleware(mongoClient) {
  const middleware = async function (req, res, next) {
    const db = createDatabase(mongoClient);

    if (!db) {
      res.status(500).send({ error: "Failed to connect to MongoDB" });
    }

    req.db = db;

    next();
  };
  return middleware;
}

function createDatabase(mongoClient) {
  const databaseName = process.env.MONGO_DATABASE_NAME;
  const database = mongoClient.db(databaseName);
  return database;
}

module.exports = { createMongodbMiddleware, createDatabase };
