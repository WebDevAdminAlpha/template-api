function createStanConnectionMiddleware(stanConn) {
  return function (res, req, next) {
    res.sc = stanConn;
    next();
  };
}

module.exports = { createStanConnectionMiddleware };
