const { validateToken } = require('../validators');

function createAuthenticationMiddleware(secret) {
    const middleware = async function (req, res, next) {
        const userId = req.params.uuid;
        const authentication = req.header("Authentication");
        const token = getToken(authentication);
        const { statusCode, errorMessage } = validateToken(userId, token, secret);

        if (errorMessage) {
            res.status(statusCode).json({ error: errorMessage });
        } else {
            next();
        }
    }
    return middleware;
}

function getToken(authentication) {
    let token = null;
    
    if (authentication) {
        token = authentication.split(' ')[1];
    }

    return token;
}

module.exports = { createAuthenticationMiddleware }